import React from "react"
import "./Checkbox.css"
import { AiTwotoneDelete } from "react-icons/ai";

export default function Checkform(props){
    return(
        <div key={props.id} className="check-form">
            <div>
                <input defaultChecked={props.thisChecked} onClick={props.onAdd} type="checkbox" id={"check "+props.id} />
                <label htmlFor={"check "+props.id}>{props.name}</label>
            </div>
            <button onClick={props.onRemove}><AiTwotoneDelete/></button>
        </div>
    )
}