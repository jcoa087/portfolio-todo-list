import { useEffect,useState } from "react"

export function useLocalStorage(key){
    const [cont,setCont] = useState(0)
    useEffect(()=>{
        const getCont= JSON.parse(localStorage.getItem(key)) ?? 0
        setCont(getCont)
      },[])
    
      useEffect(()=>{
        if(cont>0){
          localStorage.setItem(key,JSON.stringify(cont))
          setCont(cont)
        }
      },[cont])

      return[cont,setCont]
}