import "./List.css"
import { useLocalStorage } from "./useLocalStorage";
import { useCustomUseEffect } from "./useCustomUseEffect";
import React, { useEffect, useState } from "react";
import Checkform from "../checkform/Checkform"

const List = () =>{
  const key = "tasks"
  const keyDone = "done-tasks"
  const key3 = "cont"
  const [task,setTask] = useState("")
  const [tasks,setTasks] = useCustomUseEffect(key)
  const [doneTasks,setDoneTasks] = useCustomUseEffect(keyDone)
  const [cont,setCont] = useLocalStorage(key3)

  useEffect(()=>{
    if(doneTasks.length===0 && tasks.length===0) setCont(0)
  },[doneTasks,tasks,setCont])

  const addTask=()=>{
    const objTask = {
      name:task,
      id:cont
    }
    setCont(cont+1)
    setTasks([...tasks,objTask])
    setTask("")

  }
  const addTaskEnter=(e)=>{
    if(e.keyCode===13){
      const objTask = {
        name:task,
        id:cont
      }
      setCont(cont+1)
      setTasks([...tasks,objTask])
      setTask("")
    }
  }
  const removeTask=(idTask)=>{
    
    if(tasks.length===1){
      setTasks([])
      localStorage.removeItem(key)
    }else{
      const tasksUpdated = tasks.filter(task=>task.id!==idTask)
      setTasks(tasksUpdated)
    }
    
  }
  const removeDoneTask=(idTask)=>{
    
    if(doneTasks.length===1){
      setDoneTasks([])
      localStorage.removeItem(keyDone)
    }else{
      const tasksUpdated = doneTasks.filter(task=>task.id!==idTask)
      setDoneTasks(tasksUpdated)
    }
    
  }
  
  const setDone=(idDone)=>{
    const doneTask = tasks.find(x=>x.id === idDone)
    setDoneTasks([...doneTasks,doneTask])
    if(tasks.length===1){
      setTasks([])
      localStorage.removeItem(key)
    }else{
      const tasksUpdated = tasks.filter(task=>task.id!==idDone)
      setTasks(tasksUpdated)
    }
  }
  const setUnDone=(idDone)=>{
    const doneTask = doneTasks.find(x=>x.id === idDone)
    setTasks([...tasks,doneTask])
    if(doneTasks.length===1){
      setDoneTasks([])
      localStorage.removeItem(key)
    }else{
      const tasksUpdated = doneTasks.filter(task=>task.id!==idDone)
      setDoneTasks(tasksUpdated)
    }
  }
  return(
      <>
        <div className="list">    
        <div className="list-title">
          <h1>to do list</h1>
        </div>
        <div className="add-task">
            <input type="text" onChange={e=>setTask(e.target.value)} onKeyUp={addTaskEnter} value={task}/>
            <button onClick={addTask}>add</button>
          </div>
            {
              tasks.length>0?<h3>ondoing tasks ({tasks.length})</h3>:<h3>there are no tasks to do</h3>
            }
          <div className="list-tasks scroll">
            {
            tasks.map((item)=>
              <Checkform key={item.id} onAdd={()=>setDone(item.id)} onRemove = {()=>removeTask(item.id)} id = {item.id} name = {item.name} thisChecked = {false} />
            ) 
            }
          </div>   
            {
              doneTasks.length>0?<h3>done tasks ({doneTasks.length})</h3>:<h3></h3>
            }
          <div className="list-tasks scroll">
          {
            doneTasks.map((item)=>
              <Checkform key={item.id} onAdd={()=>setUnDone(item.id)} onRemove = {()=>removeDoneTask(item.id)} id = {item.id} name = {item.name} thisChecked = {true} />

            ) 
            }
          </div>
        
        </div>
      </>
  )
}
export default List