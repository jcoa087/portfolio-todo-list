import { useEffect,useState } from "react"

export function useCustomUseEffect(key){
    const [tasks,setTasks] = useState(()=>{
      const getTasks = JSON.parse(localStorage.getItem(key)) ?? []
      return getTasks
    })

    
      useEffect(()=>{
        if(tasks.length>0)localStorage.setItem(key,JSON.stringify(tasks))
        setTasks(tasks)
      },[tasks])

      return[tasks,setTasks]
}